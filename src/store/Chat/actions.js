import { SEE_ALL_MESSAGES, ADD_MESSAGE } from "./actionTypes";
import { v4 as uuidv4 } from "uuid";

const setMessagesAction = (messages) => ({
  type: SEE_ALL_MESSAGES,
  messages,
});

const setMessageAction = (message) => ({
  type: ADD_MESSAGE,
  message,
});

function sortMessages(a, b) {
  if (a.createdAt < b.createdAt) {
    return -1;
  }
  if (a.createdAt > b.createdAt) {
    return 1;
  }
  return 0;
}

export const loadMessages = (dispatch) => {
  fetch("https://edikdolynskyi.github.io/react_sources/messages.json")
    .then((res) => {
      return res.json();
    })
    .then((messages) => {
      dispatch(setMessagesAction(messages.sort(sortMessages)));
    });
};

export const setNewMessageValue = (newMessage) => (dispatch, getRootState) => {
  const {
    messages: { message },
  } = getRootState();
  console.log(message);
  console.log(newMessage);
  const editedMessage = `${message} + ${newMessage}`;
  dispatch(setMessageAction(editedMessage));
};

export const sendMessage = () => (dispatch, getRootState) => {
  const {
    messages: { messages, message },
  } = getRootState();
  // if (message) {
  let newId = uuidv4();
  let createdAt = new Date().toISOString();
  const editedAt = "";
  const userId = "me";
  const user = "Me";

  const newMessageObject = {
    id: newId,
    text: message,
    createdAt,
    editedAt,
    userId,
    user,
  };

  const editedMessages = [...messages, newMessageObject];
  dispatch(setMessagesAction(editedMessages));
  dispatch(setMessageAction(""));
  // }
};
