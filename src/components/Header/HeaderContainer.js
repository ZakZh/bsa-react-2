import React from "react";
import PropTypes from "prop-types";

import Header from "./Header";

function HeaderContainer({ chatMeta }) {
  const { chatName, userCount, messagesCount, lastMessageTime } = chatMeta;

  // function lastMessageDateGenerator(date) {

  // }

  return (
    <Header
      chatName={chatName}
      userCount={userCount}
      messagesCount={messagesCount}
      lastMessageTime={lastMessageTime}
    />
  );
}

HeaderContainer.propTypes = {
  chatMeta: PropTypes.shape({
    chatName: PropTypes.string.isRequired,
    userCount: PropTypes.number.isRequired,
    messagesCount: PropTypes.number.isRequired,
    lastMessageTime: PropTypes.string.isRequired,
  }).isRequired,
};

HeaderContainer.defaultProps = {
  chatMeta: {
    chatName: "",
    userCount: 0,
    messagesCount: 0,
    lastMessageTime: " don't added",
  },
};

export default HeaderContainer;
