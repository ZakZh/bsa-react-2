import React from "react";
import PropTypes from "prop-types";

function MessageInput({ sendMessage, setNewMessageValue, message }) {
  console.log(message + "d", setNewMessageValue);
  return (
    <div className="container fixed-bottom bg-primary mx-auto">
      <form className="row justify-content-start">
        <textarea
          className="col-10 form-control"
          id="exampleInputEmail1"
          value={message}
          onChange={(e) => setNewMessageValue(e.target.value)}
          rows="2"
        />
        <button
          type="button"
          className="btn btn-primary col-2"
          onClick={() => sendMessage()}
        >
          Send
        </button>
      </form>
    </div>
  );
}

MessageInput.propTypes = {
  sendMessage: PropTypes.func.isRequired,
  setNewMessageValue: PropTypes.func.isRequired,
  message: PropTypes.string.isRequired,
};

export default MessageInput;
