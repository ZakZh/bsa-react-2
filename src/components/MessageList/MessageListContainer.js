import React from "react";
import PropTypes from "prop-types";

import MessageList from "./MessageList";

function MessageListContainer({ messages }) {
  return <MessageList messages={messages} />;
}

MessageListContainer.propTypes = {
  messages: PropTypes.array.isRequired,
};

MessageListContainer.defaultProps = {
  messages: [],
};

export default MessageListContainer;
