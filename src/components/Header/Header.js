import React from "react";
import PropTypes from "prop-types";

function Header({ chatName, userCount, messagesCount, lastMessageTime }) {
  return (
    <div className="row justify-content-between sticky-top bg-primary p-3 m-0">
      <div className="d-inline">
        <div className="d-inline m-4">
          <strong>{chatName}</strong>
        </div>
        <div className="d-inline m-2">
          <strong>{userCount}</strong> participants
        </div>
        <div className="d-inline m-2">
          <strong>{messagesCount}</strong> messages
        </div>
      </div>
      <div className="d-inline">
        last message: <strong>{lastMessageTime}</strong>
      </div>
    </div>
  );
}

Header.propTypes = {
  chatName: PropTypes.string.isRequired,
  userCount: PropTypes.number.isRequired,
  messagesCount: PropTypes.number.isRequired,
  lastMessageTime: PropTypes.string.isRequired,
};

export default Header;
