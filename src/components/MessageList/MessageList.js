import React from "react";
import PropTypes from "prop-types";

function MessageList({ messages }) {
  let messageDate = "";
  const messagesList = messages.map((message) => {
    let currentMessageDate = new Date(message.createdAt).toDateString();
    const messageSeparator =
      currentMessageDate !== messageDate ? (
        <div className="col-12 separator">{currentMessageDate}</div>
      ) : (
        ""
      );

    messageDate = new Date(message.createdAt).toDateString();

    if (message.userId === "me") {
      return (
        <div key={message.id} className="row justify-content-end">
          {messageSeparator}
          <div
            key={message.id}
            className="row justify-content-end m-3 mx-2 p-3 border rounded"
            style={{ maxWidth: "75%", backgroundColor: "#abc1de" }}
          >
            <span>
              <strong>{message.user}</strong>
            </span>
            <p
              className="col-12 d-inline-block text-start text-break m-0"
              style={{ wordWrap: "break-word" }}
            >
              {message.text}
            </p>
          </div>
        </div>
      );
    }

    return (
      <div key={message.id} className="row justify-content-start ">
        {messageSeparator}
        <div className="m-3 p-0 w-75 d-flex">
          <div className="d-inline-block p-0" style={{ width: "10%" }}>
            <img
              src={message.avatar}
              className="w-100 border rounded"
              alt={message.user}
            ></img>
          </div>
          <div
            className="d-inline-block mx-2 p-2 border rounded"
            style={{ maxWidth: "75%", backgroundColor: "#c3dae6" }}
          >
            <span>
              <strong>{message.user}</strong>
            </span>
            <p className="m-0">{message.text}</p>
          </div>
        </div>
      </div>
    );
  });

  return (
    <div className="border bg-light" style={{ paddingBottom: "80px" }}>
      {messagesList}
    </div>
  );
}

export default MessageList;
