import { createStore, applyMiddleware, compose, combineReducers } from "redux";
import thunk from "redux-thunk";
import { loadMessages } from "./Chat/actions";

import { composeWithDevTools } from "redux-devtools-extension";

import chatReducer from "./Chat/reducer";

const initialState = {};

const middlewares = [thunk];

const composedEnhancers = compose(
  composeWithDevTools(applyMiddleware(...middlewares))
);

const reducers = {
  messages: chatReducer,
};

const rootReducer = combineReducers({
  ...reducers,
});

const store = createStore(rootReducer, initialState, composedEnhancers);

store.dispatch(loadMessages);

export default store;
