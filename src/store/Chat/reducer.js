import { SEE_ALL_MESSAGES, ADD_MESSAGE } from "./actionTypes";

export default (state = {}, action) => {
  switch (action.type) {
    case SEE_ALL_MESSAGES:
      return {
        ...state,
        messages: action.messages,
      };
    case ADD_MESSAGE:
      console.log(action.message);
      return {
        ...state,
        message: action.message,
      };
    default:
      return state;
  }
};
