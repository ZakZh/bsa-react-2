import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { v4 as uuidv4 } from "uuid";
import logo from "./logo.svg";
import HeaderContainer from "./components/Header/HeaderContainer";
import MessageListContainer from "./components/MessageList/MessageListContainer";
import MessageInputContainer from "./components/MessageInput/MessageInputContainer";
import "./Chat.css";

import {
  loadMessages,
  setNewMessageValue,
  sendMessage,
} from "./store/Chat/actions";

function Chat({ messages = [], message, sendMessage }) {
  const [loading, setLoading] = useState(false);
  const messagesCount = messages.length;
  const userCount = uniqueUsers();
  const lastMessage = messages[messages.length - 1];

  function uniqueUsers() {
    const users = new Set();
    messages.map((message) => {
      users.add(message.userId);
      return message;
    });
    return users.size;
  }

  const chatMeta = {
    chatName: "Chat",
    messagesCount,
    userCount,
    lastMessageTime: lastMessage
      ? new Date(lastMessage.createdAt).toUTCString()
      : new Date().toUTCString(),
  };

  return (
    <>
      {loading ? (
        <div className="spinner-border" role="status">
          <span className="visually-hidden">Loading...</span>
        </div>
      ) : (
        <div className="container mh-100 p-0">
          <HeaderContainer chatMeta={chatMeta} />
          <MessageListContainer messages={messages} />
          <MessageInputContainer
            sendMessage={sendMessage}
            setNewMessageValue={setNewMessageValue}
            message={message}
          />
        </div>
      )}
    </>
  );
}

MessageInputContainer.propTypes = {
  message: PropTypes.string.isRequired,
  messages: PropTypes.array.isRequired,
  sendMessage: PropTypes.func.isRequired,
  setNewMessageValue: PropTypes.func.isRequired,
};

MessageInputContainer.defaultProps = {
  messages: [],
  message: "",
};

const mapStateToProps = (rootState) => ({
  messages: rootState.messages.messages,
  message: rootState.messages.message,
});

const actions = {
  loadMessages,
  setNewMessageValue,
  sendMessage,
};

const mapDispatchToProps = (dispatch) => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
